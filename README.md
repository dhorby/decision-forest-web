# Predicting Forest Cover with Random Decision Forests using Apache Spark MLib

## Description
Am implementation of the example described by Sean Owen in the book [Advanced Analytics with Spark](http://shop.oreilly.com/product/0636920035091.do)

## Source

Scala


## Build
```
 ./gradlew build
```

## Run
```
 ./gradlew run
```

## Contact
david.horby@springer.com

