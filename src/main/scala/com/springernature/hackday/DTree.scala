package com.springernature.hackday

import org.apache.spark.mllib.evaluation.MulticlassMetrics
import org.apache.spark.mllib.linalg.Vectors
import org.apache.spark.mllib.regression.LabeledPoint
import org.apache.spark.mllib.tree.{RandomForest, DecisionTree}
import org.apache.spark.mllib.tree.model.{RandomForestModel, DecisionTreeModel}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}


class DTree {

  def getMetrics(model: DecisionTreeModel, data: RDD[LabeledPoint]):
  MulticlassMetrics = {
    val predictionsAndLabels = data.map(example =>
      (model.predict(example.features), example.label)
    )
    new MulticlassMetrics(predictionsAndLabels)
  }

  def runTree {

    val sparkConf: SparkConf = new SparkConf().setAppName("JavaWordCount").setMaster("local[2]").set("spark.executor.memory", "1g");
    val sc = new SparkContext(sparkConf)

    val rawData = sc.textFile(getClass.getClassLoader.getResource("covtype.data").getFile)
    val data = rawData.map { line =>
      val values = line.split(',').map(_.toDouble)
      val wilderness = values.slice(10, 14).indexOf(1.0).toDouble
      val soil = values.slice(14, 54).indexOf(1.0).toDouble
      val featureVector =
        Vectors.dense(values.slice(0, 10) :+ wilderness :+ soil)
      val label = values.last - 1
      LabeledPoint(label, featureVector)
    }

//    // Load the data and put it into vectors (without encoding features)
//    val rawData = sc.textFile(getClass.getClassLoader.getResource("covtype.data").getFile)
//    val data = rawData.map { line =>
//      val values = line.split(',').map(_.toDouble)
//      val featureVector = Vectors.dense(values.init)
//      val label = values.last - 1
//      LabeledPoint(label, featureVector)
//    }
    println("Count:" + data.count())

    /** Split the data into three subsets:
      *  - Training (80%)
      *  - cross-validation (CV) (10%)
      *  - test(10%)
      */

    val Array(trainData, cvData, testData: RDD[LabeledPoint]) =
      data.randomSplit(Array(0.8, 0.1, 0.1))
    trainData.cache()
    cvData.cache()
    testData.cache()

    /** Compute some metrics about the resulting model
      */

    def getMetrics(model: DecisionTreeModel, data: RDD[LabeledPoint]):
    MulticlassMetrics = {
      val predictionsAndLabels = data.map(example =>
        (model.predict(example.features), example.label)
      )
      new MulticlassMetrics(predictionsAndLabels)
    }

    // Try a single model
    val maximumDepth:Int = 4 // Maximum depth of the tree,  max number of chained decisions, limit to avoid over fitting
    val maxBinCount:Int = 100 //maximum number of bins used for splitting features
    val impurity = "gini" // Either gini or entropy
    // Gini impurity is directly related to the accuracy of the random-guess classifier
    // Entropy is another measure of impurity, borrowed from information theory, it captures how much uncertainty the collection of target values in the subset contains

    val doFirstModel = false
    if (doFirstModel) {
      val firstModel = decisionTreeTest(trainData, cvData,  impurity, maximumDepth, maxBinCount)
    }



    val mutlipleDecisionTrees = false
    if (mutlipleDecisionTrees) multipleDecisionTrees(trainData, cvData)


    val randomForest = true
    if (randomForest) {
      val forestModel = randomForestTest(trainData, cvData, getMetrics _, testData)
      forestModel.save(sc, "/Users/davidhorby/forestmodel")
    }

    val bestModel = false
    if (bestModel) {
      val maximumDepth:Int = 20 // Maximum depth of the tree,  max number of chained decisions, limit to avoid over fitting
      val maxBinCount:Int = 300 //maximum number of bins used for splitting features
      val impurity = "entropy" // Either gini or entropy

      val firstModel = decisionTreeTest(trainData, cvData,  impurity, maximumDepth, maxBinCount)
    }



//    val modelFinal = DecisionTree.trainClassifier(
//      trainData.union(cvData), 7, Map[Int,Int](), "entropy", 20, 300)
//
//    val metricsFinal = getMetrics(modelFinal, cvData)
//    println(metricsFinal.confusionMatrix)
//    println("Precision:Final" + metricsFinal.precision)
//    println("--------------------------------")
//    (0 until 7).map(
//      cat => (metrics.precision(cat), metrics.recall(cat))
//    ).foreach(println)
//    println("--------------------------------")

  }

  def randomForestTest(trainData: RDD[LabeledPoint], cvData: RDD[LabeledPoint], getMetrics: (DecisionTreeModel, RDD[LabeledPoint]) => MulticlassMetrics, testData: RDD[LabeledPoint]): RandomForestModel = {
    /* Takes 27 minutes */
    val forest = RandomForest.trainClassifier(
      trainData, 7, Map(10 -> 4, 11 -> 40), 20,
      "auto", "entropy", 30, 300)

    val labeledPredictions: RDD[(Double, Double)] = testData.map { labeledPoint =>
      val predictions = forest.predict(labeledPoint.features)
      (labeledPoint.label, predictions)
    }

    val evaluationMetrics = new MulticlassMetrics(labeledPredictions.map(x =>
      (x._1, x._2)))

    //Let’s look at the precision of the classifier—i.e. how many predictions it gets right.
    println(evaluationMetrics.confusionMatrix)
    println("Precision:Random Forest" + evaluationMetrics.precision)

    val input = "3501,125,28,67,23,3224,253,207,61,3501,0,29"
    val vector = Vectors.dense(input.split(',').map(_.toDouble))
    val result: Double = forest.predict(vector)
    println("Prediction result = " + result)
    forest

  }

  def multipleDecisionTrees(trainData: RDD[LabeledPoint], cvData: RDD[LabeledPoint]): Unit = {
    /* ---------------------------------------------------------------*/
    // Try a load of combinations (this takes 3 minutes)

    val evaluations =
      for (impurity <- Array("gini", "entropy");
           depth <- Array(1, 20);
           bins <- Array(10, 300))
        yield {
          val model = DecisionTree.trainClassifier(
            trainData, 7, Map[Int, Int](), impurity, depth, bins)
          val predictionsAndLabels = cvData.map(example =>
            (model.predict(example.features), example.label)
          )
          val accuracy =
            new MulticlassMetrics(predictionsAndLabels).precision
          ((impurity, depth, bins), accuracy)
        }
    println("Tuned Random Forest Prescision:------------------")
    evaluations.sortBy(_._2).reverse.foreach(println)
    println("Tuned Random Forest Prescision:------------------")

    /* ---------------------------------------------------------------*/
  }

  def decisionTreeTest(trainData:RDD[LabeledPoint], cvData: RDD[LabeledPoint], impurity:String, maxDepth:Int, maxBinCount:Int): DecisionTreeModel = {
    val model = DecisionTree.trainClassifier(
      trainData, 7, Map[Int, Int](), impurity, maxDepth, maxBinCount)

    val metrics = getMetrics(model, cvData)
    println(metrics.confusionMatrix)
    println("Precision:Decision Tree Forest:" + metrics.precision)
    var i:Int = 0
    (0 until 7).map(
      cat => (metrics.precision(cat), metrics.recall(cat))
    ).foreach(precision => { i += 1;println(i + ":" + precision) })
    model
  }


}



object DTree {
  def main(args: Array[String]) = {
    val dTree = new DTree().runTree
  }



}

