package com.springernature.hackday;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.Console;

@SpringBootApplication
public class DecisionForestWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(DecisionForestWebApplication.class, args);
    }

}
